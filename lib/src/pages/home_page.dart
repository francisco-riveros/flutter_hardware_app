import 'package:flutter/material.dart';
import 'package:flutter_hardware_app/src/pages/camara_page.dart';
import 'package:flutter_hardware_app/src/pages/mapa_page.dart';

class HomePage extends StatefulWidget {
  @override
  createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> { 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[350],
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: () => _camara(),
              child: Text('Camara',style: TextStyle(fontSize: 20)),
            ),
            SizedBox(height: 60),
            RaisedButton(
              onPressed: () => _mapa(),
              child: Text('Mapa', style: TextStyle(fontSize: 20)),
            ),
          ],
        ),
      ),
    );
  }

  _camara() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => CamaraPage()),);
    //Navigator.pushReplacementNamed(context, 'camara');
  }

  _mapa() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => MapaPage()),);
  }
}